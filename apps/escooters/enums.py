from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _


class LinkTypeChoices(TextChoices):
    OFFICIAL = "Ofc", _("Official")
    SERVICING = "Srv", _("Servicing")
    TESTING = "Tst", _("Testing")
    DOCUMENTS = "Doc", _("Documents")
    IMPORTANT = "Imp", _("Other important")
    OTHER = "~", _("Other")
    MENTION = "()", _("Mention only")
    OFFICIAL_VIDEO = "VOfc", _("Official video")
    TESTING_VIDEO = "VTst", _("Testing video")
    OTHER_VIDEO = "V~", _("Other video")
    MENTION_VIDEO = "V()", _("Video, mention only")
 

class WheelTypeChoices(TextChoices):
    AIR = "Air", _("Air")
    FULL = "Full", _("Full")


class ShopTypeChoices(TextChoices):
    ESHOP = "Sh", _("E-shop (autonomous)")
    MAIN = "MI", _("Main integrator store")
    SUB_VENDOR = "Su", _("Vendor inside Main integrator store")
    COUPON = "Cp", _("Person or company giving discount coupon")
    CASHBACK = "CB", _("Cashback company")
    CLASSIC = "Cl", _("Classical shop")


class OwnershipChoices(TextChoices):
    OWN = "O", _("I am the owner")
    BORROW = "B", _("Borrowed")
    TESTS = "T", _("Borrowed for tests")
