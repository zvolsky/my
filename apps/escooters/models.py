from django.db import models
from django.utils.translation import gettext_lazy as _

from django_choices_field import TextChoicesField

from .enums import (
    LinkTypeChoices,
    OwnershipChoices,
    ShopTypeChoices,
    WheelTypeChoices,
)

from apps.my_core.users import User


class Maker(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        verbose_name = _("Maker")
        verbose_name_plural = _("Makers")

    def __str__(self):
        return self.name


class MakerLink(models.Model):
    maker = models.ForeignKey(Maker, on_delete=models.CASCADE)
    link_type = TextChoicesField(max_length=4, choices_enum=LinkTypeChoices, default=LinkTypeChoices.OTHER)
    url = models.URLField()

    class Meta:
        verbose_name = _("Link to maker related webpage")
        verbose_name_plural = _("Links to maker related webpages")

    def __str__(self):
        return f"{self.link_type}: {self.url}"


class Shop(models.Model):
    name = models.CharField(max_length=96)
    typical_type = TextChoicesField(max_length=2, choices_enum=ShopTypeChoices, default=ShopTypeChoices.ESHOP)
    address = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        verbose_name = _("Shop")
        verbose_name_plural = _("Shops")

    def __str__(self):
        return f"({self.typical_type}) {', '.join(filter(None, (self.name, self.address)))}"


class CustomerService(models.Model):
    name = models.CharField(max_length=96)
    address = models.CharField(max_length=128, blank=True, null=True)
    brand = models.TextField()

    class Meta:
        verbose_name = _("Customer Service")
        verbose_name_plural = _("Customer Service Companies")

    def __str__(self):
        return self.name


class Scooter(models.Model):
    maker = models.ForeignKey(Maker, on_delete=models.CASCADE)
    name = models.CharField(max_length=96)
    weight = models.DecimalField(max_digits=4, decimal_places=1, blank=True, null=True)
    load_kg = models.SmallIntegerField(blank=True, null=True)
    height_adjustable = models.BooleanField(default=False)
    light_ahead = models.BooleanField(default=True)
    light_aback = models.BooleanField(default=True)
    light_brake = models.BooleanField(default=False)
    light_direction = models.BooleanField(default=False)
    wheel_diameter_inches = models.DecimalField(max_digits=3, decimal_places=1, blank=True, null=True)
    wheel_type = TextChoicesField(max_length=4, choices_enum=WheelTypeChoices, default=WheelTypeChoices.AIR)
    brake_electric = models.BooleanField(default=True)
    brake_foot = models.BooleanField(default=False)
    brake_mechanical_front = models.BooleanField(default=False)
    brake_mechanical_rear = models.BooleanField(default=False)
    brake_details = models.TextField(blank=True, null=True)
    engine_front_W = models.IntegerField(blank=True, null=True)
    engine_rear_W = models.IntegerField(blank=True, null=True)
    voltage_V = models.SmallIntegerField(blank=True, null=True)
    capacity_Ah = models.DecimalField(max_digits=5, decimal_places=1, blank=True, null=True)
    battery_removable = models.BooleanField(default=False)
    cruise_control = models.BooleanField(default=False)
    bluetooth = models.BooleanField(default=False)
    hints_basic = models.TextField(blank=True, null=True)
    hints_applications = models.TextField(blank=True, null=True)
    hints_other = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = _("Scooter")
        verbose_name_plural = _("Scooters")

    def __str__(self):
        return self.name


class Vehicle(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    scooter = models.ForeignKey(Maker, on_delete=models.RESTRICT)
    my_order = models.SmallIntegerField(blank=True, null=True)
    my_name = models.CharField(max_length=128, blank=True, null=True)
    owner_type = TextChoicesField(max_length=1, choices_enum=OwnershipChoices, default=OwnershipChoices.OWN)
    unboxing = models.BooleanField(default=True)
    price = models.IntegerField(blank=True, null=True)
    currency = models.CharField(max_length=3, blank=True, null=True)
    rate = models.DecimalField(max_digits=16, decimal_places=5, blank=True, null=True)
    price_eur = models.IntegerField(blank=True, null=True)
    date_from = models.DateField()
    date_to = models.DateField(blank=True, null=True)
    after_date_to = models.TextField()
    id_vehicle = models.DateField(max_length=16, blank=True, null=True)

    class Meta:
        verbose_name = _("Vehicle (my scooter)")
        verbose_name_plural = _("Vehicles")

    def __str__(self):
        return self.my_name or self.scooter.name


class VehicleShop(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    shop_type = TextChoicesField(max_length=2, choices_enum=ShopTypeChoices)
    shop = models.ForeignKey(Shop, on_delete=models.RESTRICT)

    class Meta:
        verbose_name = _("Shop (purchase)")
        verbose_name_plural = _("Shops")

    def __str__(self):
        return f"{str(self.vehicle)} {str(self.shop)} ({self.shop_type})"


class VehicleDistance(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    distance_date = models.DateField()
    distance_km = models.IntegerField()

    class Meta:
        verbose_name = _("Shop (purchase)")
        verbose_name_plural = _("Shops")

    def __str__(self):
        return f"{self.distance_date} : {self.distance_km} km"


class VehicleProblem(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    date_first = models.DateField()
    date_closed = models.DateField(blank=True, null=True)
    description = models.TextField()


class VehicleProblemNotes(models.Model):
    vehicle_problem = models.ForeignKey(VehicleProblem, on_delete=models.CASCADE)
    note_date = models.DateField()
    note = models.TextField()


class RepairVehicleProblem(models.Model):
    vehicle_problem = models.ForeignKey(VehicleProblem, on_delete=models.CASCADE)
    repair = models.ForeignKey("Repair", on_delete=models.CASCADE)
    requirement = models.TextField()
    result = models.TextField()


class Repair(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    service = models.ForeignKey(CustomerService, on_delete=models.RESTRICT)
    date_from = models.DateField()
    date_to = models.DateField(blank=True, null=True)
    vehicle_problem = models.ManyToManyField(VehicleProblem, through=RepairVehicleProblem)


class Ways(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)


# to my_core?
class Evaluation(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    evaluation_date = models.DateTimeField(auto_now_add=True)  # but read StackOverFlow
    stars = models.DecimalField(max_digits=2, decimal_places=1, blank=True, null=True)
    evaluation = models.TextField()

    class Meta:
        abstract = True


class ScooterEvaluation(Evaluation):
    shop = models.ForeignKey(Scooter, on_delete=models.CASCADE)


class ShopEvaluation(Evaluation):
    shop = models.ForeignKey(Shop, on_delete=models.CASCADE)


class ServiceEvaluation(Evaluation):
    service = models.ForeignKey(CustomerService, on_delete=models.CASCADE)

# ways
