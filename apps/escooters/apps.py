from django.apps import AppConfig


class EscootersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.escooters'
