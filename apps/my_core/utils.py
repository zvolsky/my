def to_int(src):
    """
    Will convert `src` parameter to int. If this fails, None is returned.
    """
    try:
        return int(src)
    except (ValueError, TypeError):  # TypeError occurs (at least) for src == None
        return None


def want_json(request):
    """Determine if the request need JSON response (ie. not html)"""
    accept = request.headers.get('Accept')
    if accept:
        formats = accept.split(',')
        for format in formats:
            if format == 'text/html':
                return False
    return True
