from django.conf import settings
from django.core.mail import send_mail


def test_mail(show_pwd=False, **kwargs):
    """
    kwargs example: use non deafult mail service:
        from django.core.mail import get_connection
        connection=get_connection(host=, port=, username=, password=, use_tls=, use_ssl=, ssl_keyfile=, ssl_certfile=)
    """
    if 'connection' in kwargs:
        print('explicit connection used')
    else:
        print(f"EMAIL_BACKEND : {settings.EMAIL_BACKEND}")
        if settings.EMAIL_BACKEND == 'django.core.mail.backends.smtp.EmailBackend':
            print(f"EMAIL_HOST : {settings.EMAIL_HOST}")
            print(f"EMAIL_PORT : {settings.EMAIL_PORT}")
            print(f"EMAIL_USE_TLS : {settings.EMAIL_USE_TLS}")
            print(f"EMAIL_USE_SSL : {settings.EMAIL_USE_SSL}")
            print(f"EMAIL_HOST_USER : {settings.EMAIL_HOST_USER}")
            print(f"EMAIL_HOST_PASSWORD : {settings.EMAIL_HOST_PASSWORD if show_pwd else (str(len(settings.EMAIL_HOST_PASSWORD)) + ' chars')}")

    send_mail(
        "test_mail, to seznam + gmail",
        "This is test if sending emails from server works properly.",
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=["zvolsky@seznam.cz", "mirek.zvolsky@gmail.com"],
        html_message="<html>html body</html>",
        fail_silently=False,
        **kwargs
    )
