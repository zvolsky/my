from subprocess import run

from django.conf import settings

from celery import shared_task


@shared_task
def dump_my():
    port = str(settings.DATABASES["default"]["PORT"])  # str() is not needed because it is already str, but to be sure
    with open(settings.CUSTOM_BACKUP_DIR / 'my.pgdump', "w") as outfile:
        result = run(('/usr/lib/postgresql/16/bin/pg_dump', '-p', port, '-Z4', '-Fc', 'my'), stdout=outfile)
    return result.returncode
