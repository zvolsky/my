from django.http import JsonResponse
from django.contrib.auth.mixins import LoginRequiredMixin

from .utils import want_json


class NeedAuthenticateMixin(LoginRequiredMixin):
    """Verify that the current user is authenticated."""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated and want_json(request):
            return JsonResponse({'error': 'Please Authenticate to access this endpoint.'}, status=401)
        return super().dispatch(request, *args, **kwargs)
