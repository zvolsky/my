from whitenoise.storage import CompressedManifestStaticFilesStorage


# required for settings.STORAGES,
#   as long as django-mdeditor fails with whitenoise at DEBUG = False (try on admin form with markdown field)
class NonStrictCompressedManifestStaticFilesStorage(CompressedManifestStaticFilesStorage):
    manifest_strict = False
