import json

from django.conf import settings
from django.http import HttpResponse
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.sessions.models import Session
from django.middleware.csrf import get_token
from django.views.decorators.csrf import csrf_exempt, csrf_protect, ensure_csrf_cookie


@login_required
def testing_view(request):
    return HttpResponse("This is a protected view. You are logged in!")

#@ensure_csrf_cookie
def get_csrf_view(request):
    token = get_token(request)
    response = JsonResponse({'csrf_token': token})
    # response.set_cookie(  # nevím, zda má být; převzato z CSRF middleware; každopádně nefunguje; pozor, taky je povolení cookies v prohlížeči
    #     settings.CSRF_COOKIE_NAME,
    #     request.META["CSRF_COOKIE"],
    #     max_age=settings.CSRF_COOKIE_AGE,
    #     domain=settings.CSRF_COOKIE_DOMAIN,
    #     path=settings.CSRF_COOKIE_PATH,
    #     secure=settings.CSRF_COOKIE_SECURE,
    #     httponly=settings.CSRF_COOKIE_HTTPONLY,
    #     samesite=settings.CSRF_COOKIE_SAMESITE,
    # )
    return response


# class LoginForm(forms.Form):
#     username = forms.CharField(max_length=NNN)
#     password = forms.CharField()


#@csrf_exempt
def login_view(request):
    # https://stackoverflow.com/questions/70318739/only-django-request-body-contains-data-not-request-post incl. mirek's comments
    if request.method == 'POST':
        body = request.POST or json.loads(request.body)  # .POST is empty for application/json data

        # # more sofisticated alternative using Form and form validation
        # form = LoginForm(body)
        # if form.is_valid():
        #     username = form.cleaned_data.get('username')
        #     password = form.cleaned_data.get('password')
        username = body.get('username')
        password = body.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)  # returns None ; here we have a new one row in Session model
            return JsonResponse({'message': 'Login successful'})
        else:
            return JsonResponse({'error': 'Invalid credentials'}, status=401)
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)

#@csrf_exempt
def logout_view(request):
    if request.method == 'POST':
        logout(request)
        Session.objects.filter(session_key=request.session.session_key).delete()
        return JsonResponse({'message': 'Logout successful'})
    else:
        return JsonResponse({'error': 'Method not allowed'}, status=405)
