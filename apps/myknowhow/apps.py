from django.apps import AppConfig


class MyknowhowConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.myknowhow'
