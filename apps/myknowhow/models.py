from enum import Enum

from django.db import models
from django.utils.translation import gettext_lazy as _

from mdeditor.fields import MDTextField
from modeltrans.fields import TranslationField


class TopicGroup(Enum):
    DEBIAN = 'Debian'
    GIT = 'Git'
    TOOLS_DB_API = 'Tools (database, API, env)'
    TOOLS_OTHER = 'Tools (not basic)'
    POSTGRES = 'Postgres'
    PYTHON = 'Python'
    PYTHON_INFRA = 'Python infrastructure'
    PYTHON_NUMERIC = 'Numeric and scientific Python'
    DJANGO = 'Django'
    JAVASCRIPT = 'JavaScript'
    JAVASCRIPT_INFRA = 'JavaScript infrastructure'
    SVELTE = 'Svelte'
    VUE = 'Vue'


class TopicState(Enum):
    OK = _('Ok')
    SUITABLE = _('Suitable')
    PARTIAL = _('Partial')
    BAD = _('Bad')


class Group(models.Model):
    name = models.CharField(max_length=100)
    i18n = TranslationField(fields=("name",))

    class Meta:
        verbose_name = _("Group")
        verbose_name_plural = _("Groups")

    def __str__(self):
        return self.name_i18n


class Topic(models.Model):
    name = models.CharField(max_length=100, verbose_name=_("Topic"))
    group = models.CharField(max_length=24, choices=[(group.name, group.value) for group in TopicGroup])
    groups = models.ManyToManyField(Group, verbose_name=_("Group"))
    content = MDTextField(verbose_name=_("Content"))
    prerequisites = models.TextField(null=True, blank=True)
    problems = models.TextField(null=True, blank=True)
    state = models.CharField(max_length=10, choices=[(state.name, state.value) for state in TopicState])
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("Topic")
        verbose_name_plural = _("Topics")

    def __str__(self):
        return self.name
