import graphene

from ..models import Topic
from .types import TopicType


class TopicsAllQuery(graphene.ObjectType):
    all_topics = graphene.List(TopicType)
    topic_by_id = graphene.Field(TopicType, name=graphene.String(required=True))
    #groups = graphene.Field(GroupType)

    def resolve_all_topics(self, info):
        return Topic.objects.all()

    def resolve_topic_by_id(self, info, name):
        return Topic.objects.filter(name__icontains=name).first()

    # def resolve_groups(self, info, **kwargs):
    #     return Topic.groups.all()


schema = graphene.Schema(query=TopicsAllQuery)
