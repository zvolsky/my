from graphene_django.types import DjangoObjectType

from ..models import Group, Topic


class GroupType(DjangoObjectType):
    class Meta:
        model = Group


class TopicType(DjangoObjectType):
    class Meta:
        model = Topic
        fields = ("id", "name")
 