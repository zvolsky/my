from django.contrib import admin

from modeltrans.admin import ActiveLanguageMixin

from apps.my_core.admin_patch import IModelAdmin
from .models import Topic, Group


@admin.register(Group)
class GroupAdmin(ActiveLanguageMixin, IModelAdmin):
    search_fields = ('name', 'name_i18n')


@admin.register(Topic)
class TopicAdmin(IModelAdmin):
    search_fields = ('name', 'content')
    list_filter = ('group',)
