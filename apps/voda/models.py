from django.db import models


class Furdo(models.Model):
    locality = models.CharField(max_length=32)
    public = models.BooleanField(default=True)
    image = models.ImageField(upload_to='furdok/', blank=True, null=True)

    def __str__(self):
        return self.locality
