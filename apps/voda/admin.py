from django.contrib import admin

from apps.my_core.admin_patch import IModelAdmin
from .models import Furdo


@admin.register(Furdo)
class FurdoAdmin(IModelAdmin):
    search_fields = ('locality',)
    list_filter = (
        'public',
    )
