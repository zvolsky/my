from rest_framework import generics


from ..models import Furdo
from .serializers import FurdoSerializer


class FurdokAPIView(generics.ListAPIView):
    queryset = Furdo.objects.all()
    serializer_class = FurdoSerializer


class FurdoAPIView(generics.RetrieveAPIView):
    queryset = Furdo.objects.all()
    serializer_class = FurdoSerializer
