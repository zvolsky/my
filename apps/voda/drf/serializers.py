from rest_framework import serializers

from ..models import Furdo


class FurdoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Furdo
        fields = "__all__"  # or [fld1, fld2, ..]
