from django.urls import path
# from django.views.decorators.csrf import csrf_exempt

from apps.voda.drf.views import FurdokAPIView, FurdoAPIView


urlpatterns = [
    path("furdok/", FurdokAPIView.as_view()),
    path("furdok/<int:pk>/", FurdoAPIView.as_view()),
]
