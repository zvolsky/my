from django.conf import settings
from django.db.models import Q

import graphene
from graphene_django.debug import DjangoDebug

from ..enums import Access, Season, Size
from ..models import Spa, Pool
from .types import SpaType, PoolType


class Query(graphene.ObjectType):
    spas = graphene.List(
        SpaType,
        # default is: required=False
        friendly=graphene.Boolean(),  # if no manager explicitly asked (friendly, possible, working), the default is: publish
        possible=graphene.Boolean(),
        working=graphene.Boolean(),
        name=graphene.String(),   # locality or name

        # There are many problems with Enum and this is a reason why I give a chance to Strawberry.
        # graphene.Enum fails directly, but has a workaround in graphene.Argument(graphene.Enum) wrapping.
        # However it again fails with non-uppercased entries in Enum, because graphene forces uppercase and fails.
        access=graphene.Argument(graphene.Enum.from_enum(Access)),  # graphene's Enum.from_enum() fails

        cnt_S=graphene.Int(),
    )

    def resolve_spas(self, info,
        friendly=False,
        possible=False,
        working=False,
        name=None,
        access=None,
        cnt_S=None,
    ):
        if friendly:
            manager = Spa.friendly
        elif possible:
            manager = Spa.possible
        elif working: 
            manager = Spa.working
        else:
            manager = Spa.publish
        qs = manager.prefetch_related('pool_set')
        if name:
            qs = qs.filter(Q(locality__unaccent__icontains=name) | Q(name__unaccent__icontains=name))
        if access:
            qs = qs.filter(access=access)
        if cnt_S:
            qs = qs.filter(cnt_S__gte=cnt_S)
        return qs

    # size_W = models.CharField(blank=True, null=True, **size_params)
    # cnt_W = models.SmallIntegerField(default=0)
    # is_hot = models.BooleanField(default=True)
    # highest_temperature = models.SmallIntegerField(default=37)  # 0 for non-thermal water
    # is_gyogy = models.BooleanField(default=True)  # similar to magyar standards
    # gyogy = models.CharField(**gyogy_params)
    # swimming_wo_cap = models.CharField(**season_params)
    # swimming_w_cap = models.CharField(**season_params)
    # swimming_maxsize_S = models.SmallIntegerField(blank=True, null=True)
    # swimming_maxsize_W = models.SmallIntegerField(blank=True, null=True)
    # massage_elements_S = models.CharField(**massage_elements_params)
    # massage_elements_W = models.CharField(**massage_elements_params)
    # chute = models.CharField(**season_params)
    # river = models.CharField(**season_params)
    # surge = models.CharField(**season_params)
    # massages_S = models.CharField(**service_params)
    # massages_W = models.CharField(**service_params)
    # restaurant_S = models.CharField(**service_params)
    # restaurant_W = models.CharField(**service_params)
    # fastfood_at_least_S = models.CharField(**service_params)
    # fastfood_at_least_W = models.CharField(**service_params)
    # drinks_at_least_S = models.CharField(**service_params)
    # drinks_at_least_W = models.CharField(**service_params)
    # sauna_in_price = models.CharField(**season_params)
    # sauna_surcharge = models.CharField(**season_params)

    if settings.DEBUG:
        debug = graphene.Field(DjangoDebug, name='_debug')


schema = graphene.Schema(query=Query)
