from graphene_django.types import DjangoObjectType

from ..models import Spa, Pool


class SpaType(DjangoObjectType):
    class Meta:
        model = Spa


class PoolType(DjangoObjectType):
    class Meta:
        model = Pool
 