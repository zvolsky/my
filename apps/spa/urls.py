from django.urls import path
from django.views.decorators.csrf import csrf_exempt

# from graphene_django.views import GraphQLView
# from strawberry.django.views import AsyncGraphQLView  # ASGI
from strawberry.django.views import GraphQLView  # WSGI

# from .gr_gql.schema import schema
from .strawberry.schema import schema
from .ninja.api import api


urlpatterns = [
    # path('graphql/', csrf_exempt(GraphQLView.as_view(graphiql=True, schema=schema))),  # csrf_exempt used in graphene-django docs
    path('graphql/', GraphQLView.as_view(schema=schema)),
    path("api/", api.urls),
] 
