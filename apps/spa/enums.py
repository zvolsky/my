from django.db.models import TextChoices
from django.utils.translation import gettext_lazy as _


class Access(TextChoices):
    PUB = 'Pub', _('Public')
    LIS = 'LiS', _('Private, Limited service for public')
    LIT = 'LiT', _('Private, Limited time (and maybe service) for public')
    RES = 'Res', _('Need Reservation for public')
    EMP = 'Emp', _('Accessible for public if there is free capacity')
    RER = 'ReR', _('Reservation recommended (no other limitations for public)')
    CAP = 'Cap', _('Often Capacity limitations without reservation possibility')
    PRI = 'Pri', _('Private')

access_params = {
    'max_length': 3,
    'choices_enum': Access,
    'default': Access.PUB,
}


class Gyogy(TextChoices):
    EXTR = 'Extr', _("Extrem thermal water (can cause problems)")
    HIGH = 'High', _("Well known for health effect")
    LOW = 'Low', _("Health effect middle or weak")
    THER = 'Ther', _("Thermal source, almost clear - small or unknown effect")
    NAT = 'Nat', _("Natur watter, unknown effect")
    NO = 'No', _("Conduit")

gyogy_params = {
    'max_length': 4,
    'choices_enum': Gyogy,
}


class MassageElements(TextChoices):
    YES = 'Yes', _("Yes")
    FEW = 'Few', _("Few")
    RARE = 'Rare', _("Too little")
    NO = 'No', _("No")

massage_elements_params = {
    'max_length': 4,
    'choices_enum': MassageElements,
    'default': MassageElements.NO,
}


class PoolPosition(TextChoices):
    I = 'I', _("Inner")
    O = 'O', _("Outer")
    OI = 'Oi', _("Outer, entry from inside possible")
    IO = 'Io', _("Inner with small part outside")
    M = 'M', _("Mixed")

pool_position_params = {
    'max_length': 2,
    'choices_enum': PoolPosition,
}


class Season(TextChoices):
    SUMMER = "S", _("Summer (Juny 15 - Aug)")
    LONG_SUMMER = "S+", _("Summer (May 15 - Sep)")
    WHOLE_YEAR = "W~S+", _("Whole year")
    MOUNTAIN = "WS+", _("Summer and Winter (snow) season")
    WINTER = "W", _("Winter (snow) season only")
    EXCEPT_LONG_SUMMER = "W~", _("Winter (except Summer season)")
    OFF = "_", "Off (Not present or Closed)"
    # meaning approximmately: S Juny15-Aug, + May15-Jun15&Sep, ~ Oct-Nov&Apr-May15 (out of snow), W Dec-Mar (snow)
    # to seek, look if it contains:
    # S Summer, + Summer margins, W Winter(snow), ~S WholeYear, ~ WholeYear-ExceptLongSummer(means: has summer replacement))

elements_params = {
    'max_length': 4,
    'choices_enum': Season,
    'default': Season.OFF,
}
season_params = elements_params.copy()
season_params['default'] = Season.SUMMER


class Service(TextChoices):
    YES = 'Yes', _("Yes")
    YEST = 'YesT', _("Yes, but not in margin times")
    MAY = 'May', _("Unsure")
    NO = 'No', _("No")

service_params = {
    'max_length': 4,
    'choices_enum': Service,
    'default': Service.MAY,
}


class Size(TextChoices):
    XL = 'XL', _("Extra Large")
    L = 'L', _("Large")
    M = 'M', _("Middle")
    S = 'S', _("Small")
    XS = 'XS', _("Extra Small")
    OFF = '_', _("Off (Not present or closed)")

size_params = {
    'max_length': 2,
    'choices_enum': Size,
}


class Swimming(TextChoices):
    YES = 'Yes', _("Designed for swimming, no cap required")
    CAP = 'Cap', _("Designed for swimming, cap required")
    SUIT = 'Suit', _("Suitable, but not designed for swimming")
    SEAS = 'Seas', _("Seasonaly suitable for swimming")
    LIM = 'Lim', _("Limited swimming possible (but themperature is higher)")
    NO = 'No', _("Not for swimming")

swimming_params = {
    'max_length': 4,
    'choices_enum': Swimming,
    'default': Swimming.NO,
}
