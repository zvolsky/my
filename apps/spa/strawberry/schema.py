import strawberry
import strawberry_django
from strawberry_django.optimizer import DjangoOptimizerExtension

from .filters import SpaFilter, SpaFilterLookups
from .types import Spa


@strawberry.type
class Query:
    spas: list[Spa] = strawberry_django.field(
        filters=SpaFilter,
        # order=SpaOrder,
    )
    spas_lookups: list[Spa] = strawberry_django.field(filters=SpaFilterLookups)


schema = strawberry.Schema(
    query=Query,
    extensions=[
        DjangoOptimizerExtension,
    ],
)
