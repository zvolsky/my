from typing import List

from strawberry import auto
import strawberry_django

from .. import models


@strawberry_django.type(models.Pool, fields="__all__")
class Pool:
    pass


@strawberry_django.type(models.Spa, fields="__all__")  # filters can be defined here in this parent level,
class Spa:                                               # however we prefer (at least for now) to define them at Field level
    pool_set: list[Pool]  # Pool or "Pool"

    @classmethod
    def get_queryset(cls, queryset, info, **kwargs):
        return queryset.prefetch_related('pool_set').filter(hidden=False)
