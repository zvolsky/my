from strawberry import auto
import strawberry_django
# from strawberry_django.filters import FilterLookup

from .. models import Spa


@strawberry_django.filter(Spa)
class SpaFilter:
    access: auto  # FilterLookup[str] .. lookup on single field, however I have no idea how to make it non required in query
    is_hot: auto
    is_gyogy: auto
    size_W: auto
    size_S: auto
    gyogy: auto
    swimming_wo_cap: auto
    swimming_w_cap: auto
    massage_elements_S: auto
    massage_elements_W: auto
    chute: auto
    river: auto
    surge: auto
    massages_S: auto
    massages_W: auto
    restaurant_S: auto
    restaurant_W: auto
    fastfood_at_least_S: auto
    fastfood_at_least_W: auto
    drinks_at_least_S: auto
    drinks_at_least_W: auto
    sauna_in_price: auto
    sauna_surcharge: auto

    temperature_min: int | None
    cnt_S_min: int | None
    cnt_W_min: int | None

    def filter_temperature_min(self, queryset):
        return queryset.filter(highest_temperature__gte=self.temperature_min)

    def filter_cnt_S_min(self, queryset):
        return queryset.filter(cnt_S__gte=self.cnt_S)

    def filter_cnt_W_min(self, queryset):
        return queryset.filter(cnt_W__gte=self.cnt_W)


@strawberry_django.filter(Spa, lookups=True)  # lookups on all fields, which make no problem (ie. aren't required)
class SpaFilterLookups(SpaFilter):
    pass
