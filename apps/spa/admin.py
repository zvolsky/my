from django.contrib import admin

from modeltrans.admin import ActiveLanguageMixin

from apps.my_core.admin_patch import IModelAdmin
from .models import Spa, Pool


class PoolsInline(admin.TabularInline):
    model = Pool


@admin.register(Spa)
class SpaAdmin(IModelAdmin):
    inlines = [PoolsInline]
    search_fields = ('locality', 'name')
    list_filter = (
        'hidden',
        'private_only',
        'access',
        'season',
        'size_S',
        'size_W',
        'is_hot',
        'is_gyogy',
        'swimming_wo_cap',
        'swimming_w_cap',
        'massage_elements_S',
        'massage_elements_W',
        'chute',
        'river',
        'surge',
        'massages_S',
        'massages_W',
        'restaurant_S',
        'fastfood_at_least_S',
        'drinks_at_least_S',
        'restaurant_W',
        'fastfood_at_least_W',
        'drinks_at_least_W',
        'sauna_in_price',
        'sauna_surcharge',
    )
