from django.db import models
from django.utils.translation import gettext_lazy as _

from django_choices_field import TextChoicesField


from .enums import (
    access_params,
    gyogy_params,
    massage_elements_params,
    pool_position_params,
    season_params,
    elements_params,
    service_params,
    size_params,
    swimming_params,
)
from .managers import PublishManager, WorkingManager, PossibleManager, FriendlyManager


class Spa(models.Model):
    """
    The Spa.
    For whole year opened spa the operating in summer and in winter much differs,
    but we register them still as single spa, with attribute versions for summer and winter.
    """
    objects = None
    admin = models.Manager()  # all, ie. ~ .objects
    publish = PublishManager()  # not hidden
    working = WorkingManager()  # not hidden and currently in service
    possible = PossibleManager()  # possible to visit for public guests
    friendly = FriendlyManager()  # possible to easy visit for public guests

    locality = models.CharField(max_length=64)
    name = models.CharField(max_length=128, blank=True, null=True)
    hidden = models.BooleanField(default=False)  # do not publish this Spa
    out_of_service = models.BooleanField(default=False)
    gps_S = models.CharField(max_length=24)
    gps_W = models.CharField(max_length=24)
    description = models.TextField(blank=True, null=True)
    hints = models.TextField(blank=True, null=True)
    private_only = models.BooleanField(default=False)
    access = TextChoicesField(**access_params)
    season = TextChoicesField(**season_params)
    season_note = models.TextField(blank=True, null=True)
    size_S = TextChoicesField(**size_params)
    cnt_S = models.SmallIntegerField(default=1)
    size_W = TextChoicesField(blank=True, null=True, **size_params)
    cnt_W = models.SmallIntegerField(default=0)
    open_hours = models.TextField(blank=True, null=True)
    is_hot = models.BooleanField(default=True)
    highest_temperature = models.SmallIntegerField(default=37)  # 0 for non-thermal water
    is_gyogy = models.BooleanField(default=True)  # similar to magyar standards
    gyogy = TextChoicesField(**gyogy_params)
    gyogy_note = models.TextField(blank=True, null=True)
    swimming_wo_cap = TextChoicesField(**elements_params)
    swimming_w_cap = TextChoicesField(**elements_params)
    swimming_maxsize_S = models.SmallIntegerField(blank=True, null=True)
    swimming_maxsize_W = models.SmallIntegerField(blank=True, null=True)
    swimming_note = models.TextField(blank=True, null=True)
    massage_elements_S = TextChoicesField(**massage_elements_params)
    massage_elements_W = TextChoicesField(**massage_elements_params)
    chute = TextChoicesField(**elements_params)
    river = TextChoicesField(**elements_params)
    surge = TextChoicesField(**elements_params)
    massages_S = TextChoicesField(**service_params)
    massages_W = TextChoicesField(**service_params)
    restaurant_S = TextChoicesField(**service_params)
    restaurant_W = TextChoicesField(**service_params)
    fastfood_at_least_S = TextChoicesField(**service_params)
    fastfood_at_least_W = TextChoicesField(**service_params)
    drinks_at_least_S = TextChoicesField(**service_params)
    drinks_at_least_W = TextChoicesField(**service_params)
    sauna_in_price = TextChoicesField(**elements_params)
    sauna_surcharge = TextChoicesField(**elements_params)
    sauna_note = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = _("Spa")
        # Translators: plural
        verbose_name_plural = _("Spa")

    def __str__(self):
        return ', '.join(filter(None, (self.locality, self.name)))


class Pool(models.Model):
    """
    The pool.
    For a spa with N pools we have N instances (rows) of Pool, related via foreign key `spa`.
    """
    spa = models.ForeignKey(Spa, on_delete=models.CASCADE)
    position = TextChoicesField(**pool_position_params)
    size = TextChoicesField(**size_params)
    always = models.BooleanField(default=True)
    gyogy = TextChoicesField(**gyogy_params)
    temperature = models.SmallIntegerField(default=37)
    swimming = TextChoicesField(**swimming_params)
    swimming_size = models.SmallIntegerField(blank=True, null=True)
    massage_elements = TextChoicesField(**massage_elements_params)
    chute = models.BooleanField(default=False)
    river = models.BooleanField(default=False)
    surge = models.BooleanField(default=False)
    note = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name = _("Pool")
        # Translators: plural
        verbose_name_plural = _("Pools")

    def __str__(self):
        return f"{'!' if self.always else '?'} {self.size} {self.temperature} {self.gyogy} {self.position}"

    def is_atractive_for_children(self):
        return self.chute or self.river or self.surge
