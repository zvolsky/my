from typing import List, Optional

from ninja import NinjaAPI

from apps.my_core.ninja.schema import ErrorSchema
from ..models import Spa
from .schema import SpaSchema


api = NinjaAPI()


@api.get('/spas', response=List[SpaSchema])
def spas(request, locality: Optional[str] = None):
    manager = Spa.publish
    return 200, manager.filter(locality__unaccent__icontains=locality) if locality else manager.all()


@api.get('/spas/{spa_id}', response={200: SpaSchema, 404: ErrorSchema})
def spas(request, spa_id: int):
    try:
        return 200, Spa.publish.get(pk=spa_id)
    except (Spa.DoesNotExist, Spa.MultipleObjectsReturned) as exc:
        return 404, {'message': str(exc)}
