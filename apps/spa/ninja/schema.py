from ninja import ModelSchema

from ..models import Spa


# can be based on Schema too
# class SpaSchema(Schema):
#     locality: str
#     highest_temperature: int


class SpaSchema(ModelSchema):
    class Meta:
        model = Spa
        fields = (
            'locality',
            'highest_temperature',
        )
