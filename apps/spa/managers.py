from django.db import models

from .enums import Access


class PublishManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(hidden=False)


class WorkingManager(PublishManager):
    def get_queryset(self):
        return super().get_queryset().filter(out_of_service=False)


class PossibleManager(WorkingManager):
    def get_queryset(self):
        return super().get_queryset().filter(private_only=False)


class FriendlyManager(WorkingManager):
    def get_queryset(self):
        return super().get_queryset().filter(access=Access.Pub.name)  # this is subset of private_only, so we can fork directly from WorkingManager
