from django.db import models

from apps.my_core.users import User


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    account = models.CharField(max_length=16)
    bank_code = models.CharField(max_length=6)

    def __str__(self):
        return f'{self.account} / {self.bank_code}'

    # this is possible, but not needed, because in admin:list_display we can use directly `user`(=foreignkey) and not `get_user`
    # @admin.display(description="User")
    # def get_user(self):
    #     return User.objects.filter(pk=self.id).first()


class AllowedReader(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    mail = models.EmailField(max_length=64)

    def __str__(self):
        return self.mail
