from rest_framework import viewsets
# from rest_framework.exceptions import PermissionDenied
# from rest_framework.response import Response
# from rest_framework.status import HTTP_403_FORBIDDEN

from apps.my_core.mixins import NeedAuthenticateMixin
from apps.my_core.utils import to_int
from ..models import Account
from ..permissions import IsRelatedUserPermission
from .serializers import AccountSerializer

# alternative, using separate view(s):
# from rest_framework import generics
# class AccountsAPIView(generics.ListAPIView):

class AccountViewSet(NeedAuthenticateMixin, viewsets.ModelViewSet):
    serializer_class = AccountSerializer
    permission_classes = [IsRelatedUserPermission]

    def initial(self, request, *args, **kwargs):
        self.user_id = to_int(self.request.query_params.get('u', self.request.user.id))
        return super().initial(request, *args, **kwargs)
        
    def get_queryset(self):
        queryset = Account.objects.filter(user=self.user_id)
        return queryset
