from rest_framework import permissions


class IsRelatedUserPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if not request.user.is_authenticated:
            return False
        user_id = getattr(view, 'user_id', None)
        if not user_id:
            return False  # url must contain user_id as ?u=<id>, view must parse it in initial() method and save it as view.user_id 
        if request.user.id == user_id:
            return True
        from pdb import set_trace; set_trace()
        return True  # Allow access otherwise

# TODO:
# dokončit ošetření neuatorizovaného requestu - https://gist.github.com/jerinisready/d2689745e6d76318393b2e3f2399a446
# do permission přidat AllowedReader