from django.urls import path, include
# from django.views.decorators.csrf import csrf_exempt

from rest_framework.routers import DefaultRouter

from .drf.views import AccountViewSet

router = DefaultRouter()
router.register('account', AccountViewSet, basename='account')

urlpatterns = [
    path('', include(router.urls)),
]
