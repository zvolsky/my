from django.contrib import admin

from apps.my_core.admin_patch import IModelAdmin
from .models import Account, AllowedReader


@admin.register(Account)
class AccountAdmin(IModelAdmin):
    list_display = ('user', '__str__')  # this usage of `__str__` is documented in Django docs
    list_filter = (
        'user',
    )


@admin.register(AllowedReader)
class AllowedReaderAdmin(IModelAdmin):
    list_display = ('user', '__str__')
    list_filter = (
        'user',
    )
