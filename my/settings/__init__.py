"""
(original) settings -> project -> thirdp -> (production ->) instance -> settings/__init__
"""

import os

if os.path.isfile(os.path.join('my', 'settings', 'instance_4.py')):
    from .instance_4 import *  # noqa F403
else:
    from .add3p_2 import *     # noqa F403  # default is: for development(= project_1 + add3p_2), without any changes


# here, because CUSTOM_MAILER is set by default in .project, but can be modified in .instance
local_vars = locals()
for key, value in CUSTOM_MAILER.items():
    local_vars[key] = value


# uncomment for test: Does WhiteNoise work locally and serve properly after collectstatic?
# DEBUG = False
ALLOWED_HOSTS = ["localhost", "127.0.0.1", "::1"]  # ::1 == 0:0:0:0:0:0:0:1
# ALLOWED_HOSTS = ["localhost"]  # but we use "127.0.0.1" because we need set cookies for Session Auth
