"""
This file is an instance setting. Instance means an individual installation (for development, testing, production).

instance_4_example.py is included in git repository, but unused.
Make an instance_4.py copy from it (not contained in repository) and make instance changes for current installation.

Because with missing instance_4.py the settings/__init__.py imports from .add3p_2,
    you don't need create instance_4.py if you have no changes and if you don't require the production environment.
"""

from .add3p_2 import *  # noqa F403  # for development instance (= project_1 + add3p_2), without any changes
# from .production_3 import *  # noqa F403  # for production instance


DATABASES["default"]['PORT'] = '5435'

# CUSTOM_MAILER = CUSTOM_MAILERS['seznam']  # 'seznam' commented out, because 'seznam' is the default

if DEBUG:
    INTERNAL_IPS = add_DjDT()
