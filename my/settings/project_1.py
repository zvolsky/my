import os
from django.utils.translation import gettext_lazy as _

from .settings import *  # noqa F403  # settings.py is unchanged django generated file (moved 1 level down to settings/)

# Fix BASE_DIR as soon as possible (we don't want change the auto-generated settings.py)
# With move of settings.py 1 level down (to settings/settings.py) BASE_DIR gives bad value, the my/ project dir instead of BASE_DIR.
#   Consequently all derrived paths in settings.py are corrupted, but seems it is the SQLite location only, which we do not use.
#   So we don't need repair here more.
BASE_DIR = BASE_DIR.parent
#-----------------------------------------------------------------------

INSTALLED_APPS.append("django.contrib.postgres")

INSTALLED_APPS.append("apps.my_core")
INSTALLED_APPS.append("apps.myknowhow")
INSTALLED_APPS.append("apps.spa")
INSTALLED_APPS.append("apps.escooters")
INSTALLED_APPS.append("apps.voda")
INSTALLED_APPS.append("apps.me")

MIDDLEWARE.insert(
    MIDDLEWARE.index("django.middleware.common.CommonMiddleware"),
    "django.middleware.locale.LocaleMiddleware",
)  # before


SECRET_KEY = os.getenv('MY_SECRET_KEY')


DATABASES = {
    "default": {
        # https://docs.djangoproject.com/en/5.0/ref/databases/#postgresql-notes
        'ENGINE': "django.db.backends.postgresql",
        'HOST': '',  # use `localhost` for TCP; empty is unix socket (for postgres; or use explicit: value of unix_socket_directory from postgresql.conf)
        'PORT': '5432',  # in settings/instance.py use DATABASES["default"]['PORT'] = '543n' to use other postgres version/instance
        'NAME': 'my',
        'USER': 'my',
        'PASSWORD': os.getenv('MY_DB_DEFAULT_PASSWORD'),
        # "OPTIONS": {  # this solution (ENGINE+OPTIONS) is recommended in current Django docs
        #     "service": "my_service",   # .pg_service.conf section [my_service]
        #     "passfile": "/home/mirek/.pgpass",  # .pgpass entry localhost:PORT:NAME:USER:PASSWORD
        # },
        'ATOMIC_REQUESTS': True,
    }
}


_FRONTEND = 'http://127.0.0.1:5173'  # TODO: redefine in production
# TODO: with reactive frontend, what LOGIN_URL, LOGOUT_URL means?
LOGIN_URL = f'{_FRONTEND}/accounts/login'  # TODO: redefine in production
LOGIN_REDIRECT_URL = '/admin/'
LOGOUT_URL = '/api-auth/logout/'
LOGOUT_REDIRECT_URL = '/admin/'


#X_FRAME_OPTIONS = 'SAMEORIGIN'   # requested by django-mdeditor, too, but I don't see why required? Without it: better ..check --deploy


# STATIC_URL = "static/"         # seen in the form /static/ and /media/ too (?)  # commented out, because it's already in installed settings
STATIC_ROOT = BASE_DIR / "static"
MEDIA_URL = "media/"             # requested by django-mdeditor, too
MEDIA_ROOT = BASE_DIR / "media"  # requested by django-mdeditor, too


TIME_ZONE = 'Europe/Prague'


LANGUAGES = [
    ("cs", "Čeština"),
    ("en", "English"),
]


EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
CUSTOM_MAILERS = {
    'gmail': {
        'EMAIL_HOST': 'smtp.gmail.com',
        'EMAIL_PORT': 587,  # Gmail SMTP port
        'EMAIL_USE_TLS': True,  # Gmail uses Transport Layer Security
        'EMAIL_USE_SSL': False,
        'EMAIL_HOST_USER': "mirek.zvolsky@gmail.com",
        'EMAIL_HOST_PASSWORD': os.getenv("PWD_AT_GMAIL"),
    },
    'seznam': {
        'EMAIL_HOST': 'smtp.seznam.cz',
        'EMAIL_PORT': 465,
        'EMAIL_USE_TLS': False,
        'EMAIL_USE_SSL': True,
        'EMAIL_HOST_USER': "authtemplate@seznam.cz",
        'EMAIL_HOST_PASSWORD': os.getenv("PWD_AT_SEZNAM"),
    },
}
CUSTOM_MAILER = CUSTOM_MAILERS['seznam']
DEFAULT_FROM_EMAIL = f"web server mailer <{CUSTOM_MAILER['EMAIL_HOST_USER']}>"


CUSTOM_BACKUP_DIR = BASE_DIR.parent / "data"


# případné logování chyb na konzoli (runserver + debug = True):
# LOGGING = {
#     'version': 1,
#     'disable_existing_loggers': False,
#     'handlers': {
#         'console': {
#             'level': 'ERROR',
#             'class': 'logging.StreamHandler',
#         },
#     },
#     'loggers': {
#         'django': {
#             'handlers': ['console'],
#             'level': 'ERROR',
#             'propagate': True,
#         },
#     },
# }
