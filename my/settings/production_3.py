from .add3p_2 import *  # noqa F403


DEBUG = False
ALLOWED_HOSTS = ["eventsbook.fly.dev", "aaweb.eu", "www.aaweb.eu"]
CSRF_TRUSTED_ORIGINS = ['https://eventsbook.fly.dev', 'https://aaweb.eu', 'https://www.aaweb.eu']

DATABASES["default"]['CONN_MAX_AGE'] = None
DATABASES["default"]['CONN_HEALTH_CHECKS'] = True

# DATABASES = {
#     'default': dj_database_url.config(
#         conn_max_age=600,
#         conn_health_checks=True,
#     ),
# }


EMAIL_BACKEND = "anymail.backends.sendinblue.EmailBackend"
# EMAIL_BACKEND = "anymail.backends.test.EmailBackend"


SECURE_SSL_REDIRECT = True
# https://stackoverflow.com/questions/55726610/django-ssl-redirection-on-heroku-too-many-redirects
# same problem on fly.io, solved by the HEADER: HTTP_X_FORWARDED_PROTO
# this, if missing, is not reported by `check --deploy`
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True

# browser will for 2 hours (7200 s) convert http links to https
# with expired certificate the site will look 2 hours as broken for such browser
SECURE_HSTS_SECONDS = 7200  # if still redirects to https, you can try:
    # chrome://net-internals/#hsts, Incognito mode, Settings,"Privacy and security","Clear browsing data"
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
