"""
(original) settings -> project -> thirdp

while this is named thirdp (means: add third-party libraries settings),
it is the completed setting for development
"""

from datetime import timedelta

from corsheaders.defaults import default_headers

from .project_1 import *  # noqa F403


# INSTALLED_APPS.append('django.contrib.sites')  # (seems by their docs no longer ...) mandatory for allauth

INSTALLED_APPS.append('django_extensions')
INSTALLED_APPS.append('guardian')

INSTALLED_APPS.append('allauth')
INSTALLED_APPS.append('allauth.account')  # required by 'dj_rest_auth'
INSTALLED_APPS.append('allauth.socialaccount')  # required by 'dj_rest_auth'
# INSTALLED_APPS.append('dj_rest_auth')
# INSTALLED_APPS.append('dj_rest_auth.registration')  # if present, forces checking if users email is already validated

INSTALLED_APPS.append('corsheaders')
INSTALLED_APPS.append('rest_framework')
# INSTALLED_APPS.append('rest_framework.authtoken')
## INSTALLED_APPS.append('rest_framework_simplejwt')
# INSTALLED_APPS.append('graphene_django')
INSTALLED_APPS.append('strawberry_django')  # controls the Strawberry replacement of DebugToolbarMiddleware, see bellow

INSTALLED_APPS.append('django_svelte')

INSTALLED_APPS.append('modeltrans')

INSTALLED_APPS.append('django_celery_results')
INSTALLED_APPS.append('django_celery_beat')

INSTALLED_APPS.append('anymail')

INSTALLED_APPS.append('mdeditor')


# # django.contrib.sites (for allauth):

# SITE_ID = 1


# django-allauth

MIDDLEWARE.insert(
    MIDDLEWARE.index("django.contrib.messages.middleware.MessageMiddleware") + 1,  # +1 ~ after this item
    "allauth.account.middleware.AccountMiddleware"
)

# temporary, to run initially without email system configured
ACCOUNT_EMAIL_REQUIRED = False
ACCOUNT_EMAIL_VERIFICATION = "none"

# # Provider specific settings
# SOCIALACCOUNT_PROVIDERS = {
#     'google': {
#         # For each OAuth based provider, either add a ``SocialApp``
#         # (``socialaccount`` app) containing the required client
#         # credentials, or list them here:
#         'APP': {
#             'client_id': '123',
#             'secret': '456',
#             'key': ''
#         }
#     }
# }


# dj-rest-auth

REST_AUTH = {
    # with USE_JWT=False will run as Session Authentication, thanks SESSION_LOGIN=True (TOKEN_MODEL=None)
    # You must include `rest_framework.authtoken` in INSTALLED_APPS or set TOKEN_MODEL to None

    # 'SESSION_LOGIN': True,
    "TOKEN_MODEL": None,
    # 'USE_JWT': True,
    # 'JWT_AUTH_COOKIE': 'auth',
    # 'JWT_AUTH_HTTPONLY': False,
}


# django-guardian

AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend', 'guardian.backends.ObjectPermissionBackend')


# corsheaders

CORS_ALLOW_CREDENTIALS = True

# CORS_ALLOW_ALL_ORIGINS = True
CORS_ALLOWED_ORIGINS = [
    "http://localhost:5173",
    "http://127.0.0.1:5173",
]
CSRF_TRUSTED_ORIGINS = CORS_ALLOWED_ORIGINS  # CORS is blocking without this (on 127.0.0.1)
# in case of different subdomains for backend & frontend on same common domain, things should be possible to configure using root domain:
# CSRF_COOKIE_DOMAIN = ".example.com"
# CSRF_COOKIE_HTTPONLY = False  # readable from JavaScript
# SESSION_COOKIE_DOMAIN = ".example.com"

CORS_ALLOW_METHODS = [
    'GET',
    'POST',
    'PUT',  # unused by GraphQL, the same for PATCH, DELETE
    'PATCH',
    'DELETE',
    'OPTIONS',  # Include OPTIONS if you want to handle preflight requests (ie. CORS preceding request)
]
# CORS_ALLOW_HEADERS = (
#     *default_headers,  # seems, this is ok and enough
#     # "access-control-allow-methods",  # no idea where this and the rest was collected
#     # "access-control-allow-origin",
#     # "access-control-allow-headers",
# )

# corsheaders.middleware.CorsMiddleware should be placed before everything in MIDDLEWARE which can generate response,
# ie. not only before CommonMiddleware but before WhitenoiseMiddleware too,
# we handle this later bellow together with WHITENOISE


# rest_framework

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        #'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        #'rest_framework.authentication.TokenAuthentication',
        #'rest_framework_simplejwt.authentication.JWTAuthentication',
    ],
    # Use Django's standard `django.contrib.auth` permissions,
    # or allow read-only access for unauthenticated users.
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly'
    ],
}


# djangorestframework-simplejwt

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(minutes=60),
    "REFRESH_TOKEN_LIFETIME": timedelta(days=7),
    "ROTATE_REFRESH_TOKENS": False,
    "BLACKLIST_AFTER_ROTATION": False,
    "UPDATE_LAST_LOGIN": True,
    "SIGNING_KEY": "complexsigningkey",  # generate a key and replace me
    "ALGORITHM": "HS512",
}


# GQL: graphene-django

GRAPHENE = {
    'RELAY_CONNECTION_ENFORCE_FIRST_OR_LAST': True,
    'RELAY_CONNECTION_MAX_LIMIT': 100,
    'EXTRA': {
        'GRAPHIQL': True,
    },
}


# GQL: strawberry_django

STRAWBERRY_DJANGO = {
    "FIELD_DESCRIPTION_FROM_HELP_TEXT": True,
    "TYPE_DESCRIPTION_FROM_MODEL_DOCSTRING": True,
    "GENERATE_ENUMS_FROM_CHOICES": True,
}


# whitenoise

# Shashank Gopikrishna @
#       https://stackoverflow.com/questions/34142182/insert-an-element-before-and-after-a-specific-element-in-a-list-of-strings
_after_Security = MIDDLEWARE.index("django.middleware.security.SecurityMiddleware") + 1
MIDDLEWARE.insert(_after_Security, "whitenoise.middleware.WhiteNoiseMiddleware")
MIDDLEWARE.insert(_after_Security, "corsheaders.middleware.CorsMiddleware")  # ie. before WhiteNoiseMiddleware

INSTALLED_APPS.insert(
    INSTALLED_APPS.index("django.contrib.staticfiles"),
    "whitenoise.runserver_nostatic",  # before
)
STORAGES = {
    "default": {
        "BACKEND": "django.core.files.storage.FileSystemStorage",
    },
    "staticfiles": {
        # child of whitenoise.storage.CompressedManifestStaticFilesStorage having manifest_strict = False
        #   required, as long as django-mdeditor fails with whitenoise at DEBUG = False (try on admin form with markdown field)
        "BACKEND": "apps.my_core.storages.NonStrictCompressedManifestStaticFilesStorage",
        #"BACKEND": "django.contrib.staticfiles.storage.ManifestStaticFilesStorage", # troubleshut WhiteNoise
    },
}

# celery, django-celery-results (save results to pg db), django-celery-results (task time planning)
#   setting is in settings.celery = settings/celery.py
#   v <project>/__init__.py importujeme: from <project>.settings.celery import app as celery

CELERY_BROKER_URL = 'amqp://localhost:5672'  # rabbitmq-server
#CELERY_BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'django-db'  # django-celery-results
#CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend'
CELERY_RESULT_EXTENDED = True        # django-celery-results
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
# CELERY_ACCEPT_CONTENT = ['application/json']
# CELERY_RESULT_SERIALIZER = 'json'
# CELERY_TASK_SERIALIZER = 'json'
# CELERY_TIMEZONE = 'Europe/Prague'


# django-anymail

ANYMAIL = {
    # (exact settings here depend on your ESP (Email Service Provider))
    "SENDINBLUE_API_KEY": os.getenv("SENDINBLUE_API_KEY"),
}
#DEFAULT_FROM_EMAIL = "noreply@aaweb.eu"  # if you don't already have this in settings
SERVER_EMAIL = "server_errors@aaweb.eu"  # dtto (default from-email for Django errors)
# in production.py (or cloud settings): EMAIL_BACKEND = "anymail.backends.sendinblue.EmailBackend"


# django-mdeditor

MDEDITOR_CONFIGS = {
    'default': {
        'width': '100% ',  # Custom edit box width
        'height': 500,  # Custom edit box height
        'toolbar': ["undo", "redo", "|",
                    "bold", "italic", "|",
                    "list-ul", "list-ol", "|",
                    "h1", "h2", "h3", "h5", "h6", "|",
                    "quote", "hr", "|",
                    "code", "code-block", "link", "image"
                    "||", "preview", "watch", "fullscreen"],
        # image upload format type
        'upload_image_formats': ["jpg", "jpeg", "gif", "png", "bmp", "webp", "svg"],
        'image_folder': 'editor',  # image save the folder name
        'theme': 'dark',  # edit box theme, dark / default
        'preview_theme': 'dark',  # Preview area theme, dark / default
        'editor_theme': 'pastel-on-dark',  # edit area theme, pastel-on-dark / default
        'watch': True,  # Live preview
        'lineWrapping': False,  # lineWrapping
        'lineNumbers': False,  # lineNumbers
        'language': 'en'  # zh / en / es
    }
}
# MDEDITOR_CONFIGS = {
#     'default': {
#         'width': '100% ',  # Custom edit box width
#         'height': 700,  # Custom edit box height
#         'toolbar': ["undo", "redo", "|",
#                     "bold", "del", "italic", "quote", "ucwords", "uppercase", "lowercase", "|",
#                     "h1", "h2", "h3", "h5", "h6", "|",
#                     "list-ul", "list-ol", "hr", "|",
#                     "link", "reference-link", "image", "code", "preformatted-text", "code-block", "table", "datetime",
#                     "emoji", "html-entities", "pagebreak", "goto-line", "|",
#                     "help", "info",
#                     "||", "preview", "watch", "fullscreen"],  # custom edit box toolbar
#         # image upload format type
#         'upload_image_formats': ["jpg", "jpeg", "gif", "png", "bmp", "webp", "svg"],
#         'image_folder': 'editor',  # image save the folder name
#         'theme': 'default',  # edit box theme, dark / default
#         'preview_theme': 'default',  # Preview area theme, dark / default
#         'editor_theme': 'default',  # edit area theme, pastel-on-dark / default
#         'toolbar_autofixed': False,  # Whether the toolbar capitals
#         'search_replace': True,  # Whether to open the search for replacement
#         'emoji': True,  # whether to open the expression function
#         'tex': True,  # whether to open the tex chart function
#         'flow_chart': True,  # whether to open the flow chart function
#         'sequence': True,  # Whether to open the sequence diagram function
#         'watch': True,  # Live preview
#         'lineWrapping': True,  # lineWrapping
#         'lineNumbers': True,  # lineNumbers
#         'language': 'en'  # zh / en / es
#     }
# }


# django-debug-toolbar

def add_DjDT():
    INSTALLED_APPS.append('debug_toolbar')
    if 'strawberry_django' in INSTALLED_APPS:
        MIDDLEWARE.append("strawberry_django.middlewares.debug_toolbar.DebugToolbarMiddleware")
    else:
        MIDDLEWARE.append("debug_toolbar.middleware.DebugToolbarMiddleware")

    if 'graphene_django' in INSTALLED_APPS:
        GRAPHENE['MIDDLEWARE'] = [  # additinally `debug` field in Query is required, see graphene_django docs
            'graphene_django.debug.DjangoDebugMiddleware',
        ]

    return ["127.0.0.1"]  # INTERNAL_IPS
