# to use from `celery` command as: -A "my" (or -A "my:celery") 
    # run worker: .venv/bin/celery -A "my" worker --loglevel=INFO
# instead (probably) fully qualified parameter in `celery` call can be used: -A "my.settings.celery:app" 
from my.settings.celery import app as celery
